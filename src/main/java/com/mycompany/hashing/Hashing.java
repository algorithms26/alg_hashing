/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.hashing;

/**
 *
 * @author admin
 */
public class Hashing {

    private int key;
    private String value;
    private String[] pos = new String[127]; // assume size = 127

    public Hashing(int key, String value) {
        this.key = key;
        this.value = value;
        pos[hash(key)] = value;
    }

    public int hash(int key) {
        return key % pos.length;
    }

    public String getValue(int key) {
        return (String) pos[hash(key)];
    }

    public void input(int key, String value) {
        pos[hash(key)] = value;
    }

    public String remove(int index) {
        String trash = pos[hash(index)];
        pos[hash(index)] = null;
        return trash;
    }

}
