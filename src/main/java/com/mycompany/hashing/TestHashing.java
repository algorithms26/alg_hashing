/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashing;

/**
 *
 * @author admin
 */
public class TestHashing {

    public static void main(String[] args) {
        Hashing hashing = new Hashing(198, "Jaemin"); //add values 
        hashing.input(501, "Jay");
        hashing.input(130, "Jeno\n");

        //get values from key
        System.out.println(hashing.getValue(198));
        System.out.println(hashing.getValue(501));
        System.out.println(hashing.getValue(130));
        //remove values
        System.out.println("remove values at key: 198");
        System.out.println(hashing.remove(198));
        System.out.println(hashing.getValue(198));
    }

}
